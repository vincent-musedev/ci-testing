#!/usr/bin/env bash

source .env

docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASS

docker build \
    --cache-from vincentmusedev/ci-testing-legacy:latest \
    -t vincentmusedev/ci-testing-legacy \
    "."
docker push vincentmusedev/ci-testing-legacy

docker build \
    --cache-from vincentmusedev/ci-testing-buildkit:latest \
    -t vincentmusedev/ci-testing-buildkit \
    "."
docker push vincentmusedev/ci-testing-buildkit
