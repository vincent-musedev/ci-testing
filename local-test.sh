#!/usr/bin/env bash

docker build -t vincentmusedev/ci-testing-legacy:local --cache-from vincentmusedev/ci-testing-legacy:local .
docker run --rm vincentmusedev/ci-testing-legacy:local

DOCKER_BUILDKIT=1 docker build --build-arg BUILDKIT_INLINE_CACHE=1 -t vincentmusedev/ci-testing-buildkit:local --cache-from vincentmusedev/ci-testing-buildkit:local .
docker run --rm vincentmusedev/ci-testing-buildkit:local
