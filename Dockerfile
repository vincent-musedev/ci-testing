FROM alpine

ENV DOCKERFILE_VERSION=1

RUN date > date.txt

COPY output.txt .

CMD echo "Output v$DOCKERFILE_VERSION @ $(cat date.txt): $(cat output.txt)"
