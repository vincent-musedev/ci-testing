#!/usr/bin/env bash

source .env

HUB_TOKEN=$(curl -s -H "Content-Type: application/json" -X POST -d '{"username": "'${DOCKERHUB_USER}'", "password": "'${DOCKERHUB_PASS}'"}' https://hub.docker.com/v2/users/login/ | jq -r .token)
echo $HUB_TOKEN

curl -i -X DELETE \
  -H "Accept: application/json" \
  -H "Authorization: JWT $HUB_TOKEN" \
  https://hub.docker.com/v2/repositories/vincentmusedev/ci-testing-legacy/tags/latest/

curl -i -X DELETE \
  -H "Accept: application/json" \
  -H "Authorization: JWT $HUB_TOKEN" \
  https://hub.docker.com/v2/repositories/vincentmusedev/ci-testing-buildkit/tags/latest/

curl -i -X DELETE \
  -H "Accept: application/json" \
  -H "Authorization: JWT $HUB_TOKEN" \
  https://hub.docker.com/v2/repositories/vincentmusedev/ci-testing-legacy/tags/local/

curl -i -X DELETE \
  -H "Accept: application/json" \
  -H "Authorization: JWT $HUB_TOKEN" \
  https://hub.docker.com/v2/repositories/vincentmusedev/ci-testing-buildkit/tags/local/
